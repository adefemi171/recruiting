### LinkedIn Access Request


**1.  What seat type are you requesting?**

* [ ]  LinkedIn *Hiring Manager* seat
* [ ]  LinkedIn *Recruiter* seat

**2.  What's your GitLab email address?**

*  [ADD EMAIL HERE]

**3.  Have you added your GitLab email address to your LinkedIn profile?**

* [ ]  Yes
* [ ]  No
    
To add your email, click *Me* > *Settings & Privacy* > *Account* > *Email addresses* > add your GitLab email address and verify it

---

### Provisioners Only Section:

#### People Group

* [ ]  @ewegscheider, check-off and close `Issue` upon provisioning seat

---
### Do Not Edit Below

Changes to this template must be approved by a People Group team member

/label ~"NewAccessRequest" ~"peopleops::to do" ~"ReadyForProvisioning"
/assign @ewegscheider
