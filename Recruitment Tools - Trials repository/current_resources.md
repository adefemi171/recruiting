# **Purpose:**

This page is list of all the systems and tools that are currently in use.

# **Sourcing Tools:**

[**ContactOut**](https://gitlab.com/gitlab-com/finance/issues/683#note_178270583)
* **Key Features:** Browser plug-in for finding candidate email addresses and phone numbers
* **Evaluators:** Alina, Anastasia, Chris, Erich, Kanwal, Viren, Zsuzsanna
* **Vendor POC:** Ryan Holly; ryan@contactout.io
* **Pricing:** $8,400 per year
* **Decision:** `Renewed`

# **Applicant Tracking Systems:**

[**Greenhouse**](https://gitlab.com/gitlab-com/finance/issues/671)
* **Key Features:** BI Connector, Inclusion, Recruiting 
* **Evaluators:** Recruiting Team
* **Vendor POC:** Jackie Guselli; jguselli@greenhouse.io
* **Pricing:** $39,097.95 per year
* **Decision:** `Renewed`

# **Job Boards:**

- n/a

# **Available Resources:**

| **Resources**        | **Business Purpose**      | **Internal Contact** |
| -------------------- | ------------------------- | -------------------- |
| Calendly             | Meeting Scheduling        | TBD                  |
| ContactOut           | Emails and Phone Numbers  | TBD                  |  
| DocuSign             | Digital Signatures        | Erich W.             | 
| Greenhouse           | Applicant Tracking System | Erich W.             |
| LinkedIn             | Sourcing                  | Erich W.             |
| Zoom                 | Video Conferencing        | People Ops           |
