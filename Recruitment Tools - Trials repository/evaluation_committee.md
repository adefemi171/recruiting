# **Purpose:**

This page contains a list of the evaluation committee for People Team related systems and tools.

# **People Team:**

| **Evaluator**          | **Role**                            |
| ---------------------- | ----------------------------------- |
| Nadia Vatalidis        | Manager, People Operations          |
| Cassiana Gudgenov      | People Operations Specialist        |
| Trevor Knudsen         | People Operations Specialist        |

# **Recruiting Team:**

| **Evaluator**          | **Role**                            |
| ---------------------- | ----------------------------------- |
| Kelly Murdock          | Manager, Recruiting                  |
| Erich Wegscheider      | Senior Talent Operations Specialist |
| April Hoffbauer         | Manager                  |

# **Sourcing Team:**

| **Evaluator**          | **Role**                            |
| ---------------------- | ----------------------------------- |
| Anastasia Pshegodskaya | Sourcing Manager                    |
| Alina Moise            | Senior Recruiting Sourcer           |
| Chris Cruz             | Senior Technical Sourcer            |
| Kanwal Matharu         | Sourcer                             |
| Susan Hill             | Senior Sourcer                      |
