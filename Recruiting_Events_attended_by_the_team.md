# **Purpose:**

This page is created to document all the information about the Recruiting events we've attended and share our feedback.

Please submit your feedback in the following format:

* **Name of the event:**
* **Website:**
* **Attendees:**
* **Role of the attendees (speaker/organizer/participant):**
* **Location:**
* **Date:**
* **Does this event require additional budget (tickets, accomodation, transportation, etc):**
* **Link to the event approval request issue:**
* **Would you recommend attending this event in future? Please justify your answer.**


# **Events attended**

## Women in Tech, Brno
* **Website:** https://womenintech.cz/
* **Attendees:** @apshegodskaya
* **Role of the attendees (speaker/organizer/participant):** `Speaker`
* **Location:** Brno, Czech Republic
* **Date:** June 2018
* **Does this event require additional budget (tickets, accomodation, transportation, etc):** `Yes` for everyone except speakers
* **Link to the event approval request issue:** n/a
* **Would you recommend attending this event in future? Please justify your answer.** `Yes`. The biggest diversity event in the Czech Republic with a large number of female attendees. Most of the participants were fresh graduates or students.

## Sourcing Summit, Germany
* **Website:** https://sourcingsummit.de/en/about/
* **Attendees:** @apshegodskaya, @zkovacs
* **Role of the attendees (speaker/organizer/participant):** `Participants`
* **Location:** Munich, Germany
* **Date:** April 2019
* **Does this event require additional budget (tickets, accomodation, transportation, etc):** `Yes`
* **Link to the event approval request issue:** https://gitlab.com/gitlab-com/people-ops/recruiting/issues/91
* **Would you recommend attending this event in future? Please justify your answer.** `Yes`. A lot of hands-on experience and takeaways, great speakers, high quality of content, that was mostly related to tech sourcing. This event is definitely valuable for those who are interested in mastering their sourcing skills.

## Sourcing Summit, Estern Europe
* **Website:** https://sourcingsummit.ee/about-2/
* **Attendees:** @apshegodskaya
* **Role of the attendees (speaker/organizer/participant):** `Speaker`
* **Location:** Tallinn, Estonia
* **Date:** June 2019
* **Does this event require additional budget (tickets, accomodation, transportation, etc):** `Yes` for everyone except speakers
* **Link to the event approval request issue:** https://gitlab.com/gitlab-com/people-ops/recruiting/issues/92#
* **Would you recommend attending this event in future? Please justify your answer.** `Yes`. This is the same type of event as mentioned above. The organizers offere a variety great workshops, great speakers, and even a sourcing hackathon.

## LinkedIn Talent Connect
* **Website:** https://www.talentconnect2019.com/
* **Attendees:** @apshegodskaya, @Ahoffbauer
* **Role of the attendees (speaker/organizer/participant):** `Participants`
* **Location:** Dallas, USA
* **Date:** September 2019
* **Does this event require additional budget (tickets, accomodation, transportation, etc):** `Yes`
* **Link to the event approval request issue:** https://gitlab.com/gitlab-com/people-ops/recruiting/issues/163
* **Would you recommend attending this event in future? Please justify your answer.** `No`. The content was very high-level and was mostly focused on upselling LinkedIn products rather than sharing some practical knowledge.

## GitLab Commit, London 
* **Website:** https://about.gitlab.com/events/commit/ 
* **Attendees:** @rdouglas-gitlab
* **Role of the attendees (speaker/organizer/participant):** Participant 
* **Location:** London
* **Date:** 2019.10.09
* **Does this event require additional budget (tickets, accomodation, transportation, etc):** No
* **Link to the event approval request issue:** 
* **Would you recommend attending this event in future? Please justify your answer.** Yes, lots of highly-engaged GitLab users and attendees who are intruiged by our working pratices. 
 
## Glassdoor Recruit, SF
* **Website:** https://www.glassdoor.com/employers/events/recruit/
* **Attendees:** @Ahoffbauer
* **Role of the attendees (speaker/organizer/participant):** `Participants`
* **Location:** SF, USA
* **Date:** October 2019
* **Does this event require additional budget (tickets, accomodation, transportation, etc):** `Yes`
* **Link to the event approval request issue:** https://gitlab.com/gitlab-com/people-group/recruiting/issues/187
* **Would you recommend attending this event in future? Please justify your answer.** `Yes`. The speakers were engaging and informative and there were no break out sessions so you didn't feel like you were missing out on anything. Glassdor was aquired by Recruit so I'm curious if this event changes next year. 
