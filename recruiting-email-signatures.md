# Email signatures for recruiting 

## Notes

* Related issue [here.](https://gitlab.com/gitlab-com/people-ops/recruiting/issues/192) 
* General instructions in the handbook for creating an email signature can be found [here.](/handbook/tools-and-tips/#email-signature) 
* Please consider these all suggestions and make your signature your own! 

## Things to consider adding to your signature

### Your location 

If it’s not already on your signature, including your location is helpful to show how global our team is today. 

### Your LinkedIn or Twitter profile

Choose a channel you use for recruiting and link to it under your title.  

E.g. “Let’s connect on LinkedIn” with hyperlink 

E.g. “Follow me on Twitter” with hyperlink 

### Tagline or short blurb about GitLab hiring

See examples below. If it makes more sense for the team you support, you could include a more product-related tagline. 

**Join our global, all-remote team.** <br>
[Browse jobs](https://about.gitlab.com/jobs/) | [About GitLab](https://about.gitlab.com/) | [Our handbook](https://about.gitlab.com/handbook/)

**We’re hiring. Learn more about life at GitLab.** <br>
[Browse jobs](https://about.gitlab.com/jobs/) | [About GitLab](https://about.gitlab.com/) | [Our handbook](https://about.gitlab.com/handbook/)

**Help the world build better software.** <br>
[Browse jobs](https://about.gitlab.com/jobs/) | [About GitLab](https://about.gitlab.com/) | [Our handbook](https://about.gitlab.com/handbook/)

### Links 

Add 2-4 related hyperlinks about life at GitLab, our vacancies, the team you support, etc. Here are a few other suggestions of places to link: 
* [Vacancies](https://about.gitlab.com/jobs/) (jobs page)
* [Handbook](https://about.gitlab.com/handbook/)
* [GitLab Culture](https://about.gitlab.com/company/culture/)
* [All-remote work](https://about.gitlab.com/company/culture/all-remote/)
* [Our values](https://about.gitlab.com/handbook/values/#credit) 
* [Products](https://about.gitlab.com/products/) 
* (handbook link or gitlab.com page for team you support)

**Example screenshots in [this doc.](https://docs.google.com/document/d/1RxvjTo2KVDt0eesfUslTQx33acfDrAVygBj0PPtFgvI/edit?usp=sharing)**





